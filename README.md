# git-instaweb-wrapper

## _Set up gitweb to serve a list of git repos_

&hellip; or even run as a [runit](http://smarden.org/runit/)-style service

`git instaweb` is extremely convenient, but not very configurable. `gitweb` itself is extremely configurable, but not so convenient to incorporate in a webserver.

`instaweb-wrapper` takes care of the initial gitweb webserver setup. It calls `instaweb` to generate an initial configuration, then modifies it so that it works standalone, and copies it out of the wrapper project's `.git`. It also adds a script to run gitweb (the enclosing webserver, that is) as a `runit` / `runsv` service. You can then track your wrapper repo, including the webserver configuration, and possibly the list of projects, in `git`.

## Installing

- create an empty wrapper folder (or clone `git-instaweb-wrapper` itself)
- [optional] symlink the tracked projects (possibly inside a `repos/` folder, to be easily `.gitignore`'d)
- call `git-instaweb-wrapper/setup` with either `--dir=repos`, or a list of repo dirs / symlinks, as arguments. None of the arguments need to be inside the wrapper repo. This will prepare a `gitweb` folder and initialize `git` if necessary.

Now that your wrapper project is configured:

- run `gitweb/run` and point your browser to `http://127.0.0.1:1234/`
- tweak the generated config files as you like.

To control the server via [`runit`](http://smarden.org/runit/)'s `runsv`, install it (`apt install runit`) and call `gitweb/run` with the `-d` (daemonize) / `--sv=` options (see below). `runsv-ctl` can also use [`daemonize`](https://software.clapper.org/daemonize/) if called with `--daemonize` (instead of `-d`) &mdash; though if we're talking server-level robustness, you may wish to set up a "real" `runit` service.

Script options (may not be up to date &mdash; call `setup --help`):

```
Usage: setup [OPTION]... [REPO]...
Options:
-h | --help
-v | --verbose
-y | --yes
--dir=DIR
--owner=OWNER
--srvdir=SRVDIR

Detects server dir from git, or --srvdir
--dir=repos is the same as calling with repos/*/
```

After setup: `./gitweb/runsv-ctl --help`
```
Usage:
runsv-ctl [ -d | --sv={ '' | 'SV-ARG...' } | --tail ]
  --sv= (no SV-ARG) starts runsv; -d starts a daemonized runsv
  See man [run]sv(1) for SV-ARG (up, down, term, exit, once, stat, -v etc.)

runsv-ctl [--] [SERVER-ARG... ]
  Start './run' (stop with ^C)
```

## TODO
- support other webservers (`python`). Currently only `lighttpd` is supported.
